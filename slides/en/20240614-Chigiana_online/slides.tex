\providecommand{\pmode}{beamer} % fallback to beamer presentation
\documentclass[\pmode]{beamer}
\usepackage{gitinfo2}
\usepackage{varioref}
\usepackage{setspace}
\usepackage[italian]{babel}
\usepackage{tabularx}
\usepackage{xspace}
\usepackage{tabularray}
\usepackage{circledsteps}
\usepackage{hyperref}

\newcommand{\handoutpresentation}{handout}
%\ifx\pmode\handoutpresentation
\mode<handout>{%
	\usepackage{handoutWithNotes}
	\pgfpagesuselayout{2 on 1 with notes}[a4paper,border shrink=1mm,landscape]
}
%\fi
\mode<handout>{\setbeamercolor{background canvas}{bg=black!2}}

\newcommand{\rootdir}{../..}
\newcommand{\imagedir}{\rootdir/images}
\newcommand{\exampledir}{\rootdir/examples}
\newcommand{\lilydir}{\rootdir/images/ly-out}

\input{\rootdir/macros}

% added gitinfo2 marks

\title{\Man's\\\Plut (1988)\\A short presentation\\[-0.25\baselineskip] {\tiny (\versiontag)}}
\author{Nicola Bernardini}
\date{June 14, 2024}
\institute[Chigiana]{Accademia Chigiana -- Seminario Live--Electronics and Sound \& Music Computing}

\begin{document}

\begin{frame}
  \titlepage
  
  \makebox[\textwidth][l]{%
      \makebox[0.1\textwidth][l]{\includegraphics[width=0.1\textwidth]{\imagedir/cc}}
      \parbox[l]{0.9\textwidth}{
      \begin{spacing}{0.5}
	 {\tiny These slides are released under the Creative Commons
	 Attribution -- ShareAlike 4.0 License. To read the licence text in
	 full please visit \url{http://creativecommons.org/licenses/by-sa/4.0/en/} or
         send a mail to Creative Commons, 171 Second Street, Suite 300, San
         Francisco, California, 94105, USA.}
       \end{spacing}
     }
  }
\end{frame}

\section{Context}

\newcounter{ms}
\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Man's \Plut -- context (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

  	\item Philippe Manoury is a French composer born in 1952

	\item composition studies with G\'erard Cond\'e and Max Deutsch first
	      and then Michel Philippot, Ivo Malec, and Claude Ballif.

	\item in 1975, studies in computer assisted composition with Pierre Barbaud

	\item in 1980, joins IRCAM as a composer and electronic music researcher

	\item at IRCAM begins a life--long collaboration with
	musician/mathematician Miller Puckette which will continue when
	Manoury will teach at UCSD San Diego after 1992

  \end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Man's \Plut -- context (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

	\item \Plut is one of a number of works stemming from the
	collaboration of composer Philippe Manoury and musician/mathematician
	Miller Puckette. Other works are:

  	\begin{itemize}[<+- | alert@+>]

		\item \emph{Jupiter}, op.15a (1987 -- revised 1992), for flute
		      and live--electronics

		\item \emph{La Partition du Ciel et de l'Enfer}, op.19 (1989)

		\item \emph{Neptune}, op.21 (1991), for 3 percussion players
		and live--electronics

  	\end{itemize}

	\uncover<+->{These works are part of a series called \emph{Sonus ex machina}.}

  \end{itemize}

\end{frame}

\section{\Plut}

\subsection{Notes}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Plut -- (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

	\item technically, \Plut is a work for piano retrofitted with
	      MIDI transducers on the keys and real--time synthesis and
	      processing systems

  \end{itemize}
  \uncover<+->{
     \begin{center}
	\begin{figure}
		\pgfimage[width=0.9\textwidth]{\imagedir/pluton_titling.png}
 		\caption{\ref{fig:score_title} \Plut title\label{fig:score_title}}
	\end{figure}
     \end{center}
  }
  \begin{itemize}[<+- | alert@+>]

    \item it is a work dedicated to Pamela and Miller Puckette

  \end{itemize}

\end{frame}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Plut -- Notes of the composer (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

	\item \Plut is a work for piano retrofitted with
	      MIDI transducers on the keys and real--time synthesis and
	      processing systems

        \item At the moment of its creation, these systems included the
	\emph{4X} (built at IRCAM by Giuseppe Di Giugno) controlled by the
	program Max written by Miller Puckette running on a Macintosh.

	\item Currently (i.e. in 1993) the totality of programs has been
	integrated by Miller Puckette in the \emph{NeXT}--driven workstation \emph{ISPW} 
	developed by the team of Eric Lindeman.

	\item Cort Lippe has developed several elements of this environment

  \end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Plut -- Notes of the composer (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

	\item The electronic score serves only as a reference for the piano
	      player and it does not constitute a complete score, which would
	      be unrealizable in traditional notation

	\item a score follower, integrated into the system, allows the
	real--time system to synchronize with the soloist

	\item the triggered events are represented in the score by circled
	numbers (\Circled{1}, \Circled{2}, \Circled{3}, \ldots, etc.)
	which correspond to the processing programs

  \end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Plut -- Notes of the composer (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

	\item Synthesis and processing programs include:

  	\begin{itemize}[<+- | alert@+>]

		\item {\bfseries 1 Frequency--shifter} with positive (\emph{+}) or negative (\emph{-}) output

		\item {\bfseries 1 Harmonizer} (written ``Harm'' with an added
		``Del'' when used with delay), feedback, window, transposition
		interval (major third, minor sixth, \ldots) and phasing when
		the interval is very small

		\item {\bfseries 2 Reverberators} (written ``Rev'' or ``Reverb'', also with infinite reverberations
			``Reverb $\infty$'')

		\item {\bfseries 2 Samplers} (written ``Sampling'') allowing
		identical, transposed, spectral--shifted or time--shifted re--playing 

		\item {\bfseries 36 additive synthesis Oscillators} (written
		``Osc'') controlled by a spectral (FFT) analysis of the piano

		\item {\bfseries 1 Spatializer} allowing a 2 or 4--channel output with rotations at different speeds

  	\end{itemize}

  \end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Plut -- Notes of the composer (\arabic{ms})}

  \begin{center}
  	\begin{figure}
		\pgfimage[height=0.75\textheight]{\imagedir/pluton-disposition.png}
 		\caption{\ref{fig:loudspeaker disposition} \Plut loudspeaker disposition\label{fig:loudspeaker disposition}}
  	\end{figure}
  \end{center}

\end{frame}

\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Plut -- Notes of the composer (\arabic{ms})}

  \begin{center}
  	\begin{figure}
		\pgfimage[height=0.75\textheight]{\imagedir/pluton-system.png}
 		\caption{\ref{fig:processing system} \Plut processing system\label{fig:processing system}}
  	\end{figure}
  \end{center}

\end{frame}

\subsection{Form}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Plut -- General Form (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

	\item \Plut is a single--movement, ca. 50 min. work divided in 5 large
	      sections ({\bfseries I} to {\bfseries V}) with subdivisions:

  	\begin{itemize}[<+- | alert@+>]

		\item {\bfseries Ia}--{\bfseries Ib}

		\item {\bfseries IIa}--{\bfseries IId}--{\bfseries IIe}--{\bfseries IIf}

		\item {\bfseries III}: ``open'' section (see \vref{secIII})

		\item {\bfseries IVa}--{\bfseries IVb}--{\bfseries IVc}--{\bfseries IVd}

		\item {\bfseries Va}--{\bfseries Vb}--{\bfseries Vc}--{\bfseries Vd}--{\bfseries Ve}--{\bfseries Vf}

  	\end{itemize}

  \end{itemize}

\end{frame}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Plut -- Section III (\arabic{ms})\label{secIII}}

  \begin{itemize}[<+- | alert@+>]

	\item section {\bfseries III} is a set of instructions that must be
	assembled by the performer with some degree of freedom. It is built
	out of the following components:

  	\begin{itemize}[<+- | alert@+>]

		\item nine ``record'' fragments ({\bfseries R1}--{\bfseries R9})

		\item five ``markovian'' fragments ({\bfseries A1}--{\bfseries A5})

		\item three ``functional'' fragments ({\bfseries E}, {\bfseries M}, {\bfseries S})

  	\end{itemize}

	\item The section \emph{must} begin by fragment {\bfseries R1} and end by fragment {\bfseries S}

  \end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\Plut -- Section III (\arabic{ms})}

	\begin{itemize}[<+- | alert@+>]

		\item The fragments have some linking constraints:

	\begin{itemize}[<+- | alert@+>]

	 	\item the {\bfseries R} fragments have to appear in order ({\bfseries R1} to {\bfseries R9})

		\item the ``functional'' fragments have an opening and a
		closing note; the closing note is mandatory when linking to
		{\bfseries R} or {\bfseries A} sequences, but not when linking
		to another ``functional'' fragment

		\item the ``markov'' fragments may appear in any order interspersed within the other fragments

		\item an example order provided by in the score:
		\begin{figure}
			\pgfimage[width=0.9\textwidth]{\imagedir/pluton-sectionIII-ex.png}
	 		\caption{\ref{fig:score_secIII} \Plut Section III example\label{fig:score_secIII}}
		\end{figure}

  	\end{itemize}

  \end{itemize}

\end{frame}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\Plut -- Lights (\arabic{ms})}

	\begin{itemize}[<+- | alert@+>]

		\item there are some indications for staging and lights:

	\begin{itemize}[<+- | alert@+>]

		\item ``There are lighting changes at the beginning and end
		       of the piece which should be rehearsed with the lighting operator.''

		\item ``The piece begins in complete darkness. The pianist walks on stage
		        in the darkness (the audience should not be able to see that this is happening;
			this is easier to accomplish than it sounds as the audience's eyes won't be adjusted to the darkness.)''

		\item ``The piece ends with the computer making a two-minute
		time stretch of a piano sample.  As the sample begins playing,
		the lights fade out quite slowly, perhaps over a minute.
		The pianist then leaves the stage in complete darkness.''

		\item ``When the time-stretched sample ends, the lights go back up (medium speed)
		      to reveal the piano and empty bench. (Other, unprocessed sampled piano sounds
		      are still coming from the speakers).'' 

		\item ``The computer operator stops the patch (hitting the "mute" button is good enough),
		        and the pianist walks back onstage to bow.''

  	\end{itemize}

  \end{itemize}

\end{frame}

\section{\pd}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Plut -- \pd (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

	\item the composer's notes translate quite directely into the
	      \pd patch that is used today

  \end{itemize}

  \begin{center}
	\pgfimage[width=0.8\textwidth]<2>{\imagedir/pluton-pure_data-0.png}
	\pgfimage[width=0.8\textwidth]<3>{\imagedir/pluton-pure_data-1.png}
	\pgfimage[width=0.8\textwidth]<4>{\imagedir/pluton-pure_data-2.png}
	\pgfimage[width=0.8\textwidth]<5>{\imagedir/pluton-pure_data-3.png}
	\pgfimage[width=0.8\textwidth]<6>{\imagedir/pluton-pure_data-4.png}
  \end{center}

\end{frame}

\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Plut -- \pd (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

	\item Analyzing the patch, we can observe some interesting patterns:

  	\begin{itemize}[<+- | alert@+>]

		\item the patch is \emph{monolithic}: (almost) everything is
		self--contained

		\item lots of specialized sub-patches

		\item few abstractions

		\item even fewer specialized objects
		     (3: \texttt{scofo}, \texttt{ragout} and \texttt{snarkov})

  	\end{itemize}

	\item simplicity is preferred over structure

  \end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Plut -- \pd (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

	\item everything is very liberally licensed (MIT License), so
	everything concerning the software of \Plut is publicly available
	to study and to use

	\item everything, at least in principle, works out of the box

	\item this work and the works related to the
	      \href{https://msp.ucsd.edu/pdrp/}{\texttt{pure data} Repertory
	      Project (pdrp)} are all available online always under the MIT
	      License

	\item this constitutes a very good example of long--term sustainability
	      of electroacoustic music

  \end{itemize}

\end{frame}

  % simulator (download link for the performance)

\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Plut -- a simulator (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

	\item instructions to use the simulator:

  	\begin{itemize}[<+- | alert@+>]

		\item download the performance file from
		\href{https://msp.ucsd.edu/tmp/pluton-record4956.wav}{here}

		\item copy (or symlink) it in the path \texttt{simulator/pluton.wav}

		\item start the \Plut \texttt{pluton.pd} patch and press the start button at the bottom

		\item start the \texttt{simulator.pd} patch and press the ``start all'' green button

		\item enjoy

  	\end{itemize}

  \end{itemize}

\end{frame}


\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{Sources for this presentation}

	\begin{center}
		\url{https://gitlab.com/nicb/Manourys_Pluton-A_short_introduction}
	\end{center}

\end{frame}

\end{document}
